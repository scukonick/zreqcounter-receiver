package main

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"syscall"
	"time"

	log "github.com/Sirupsen/logrus"
)

type FileState struct {
	Position int64
	FilePath string
	FileInfo os.FileInfo `json:"-"`
	Inode    uint64
}

type StringWrapper struct {
	line string
	meta map[string]string
}

func (fileState *FileState) Save() {
	// Inode processing
	// stat := *fileState.FileInfo.Sys().(*syscall.Stat_t)
	// fileState.Inode = stat.Ino
	// Saving information about file
	md5Filename := md5.Sum([]byte(fileState.FilePath))
	filename := hex.EncodeToString(md5Filename[:])
	log.Debugf("Saving filestate of %s to file: %s", fileState.FilePath, filename)
	jsonedFileState, err := json.Marshal(fileState)
	if err != nil {
		log.Errorf("Oops, could not jsonize filestate: %s", err)
		return
	}

	log.Debugf("filename: %s", filename)
	err = ioutil.WriteFile(filename, jsonedFileState, 0644)
	if err != nil {
		log.Errorf("Oops, error when writing file state: %s", err)
		return
	}
}

func getIno(fi os.FileInfo) uint64 {
	stat := fi.Sys().(*syscall.Stat_t)
	return stat.Ino
}

func getFileState(filename string) (*FileState, error) {
	md5Filename := md5.Sum([]byte(filename))
	filestateName := hex.EncodeToString(md5Filename[:])
	f, err := ioutil.ReadFile(filestateName)
	if err != nil {
		log.Debugf("No file state file for %s", filename)
		return nil, errors.New("no file state file")
	}
	fileState := &FileState{}
	err = json.Unmarshal(f, fileState)
	if err != nil {
		log.Errorf("Could not read json from filestate file for %s, err: %s", filename, err)
		panic(err)
		return nil, errors.New("could not read Json")
	}
	return fileState, nil

}
func check(e error) {
	if e != nil {
		panic(e)
	}
}
func getInode(filename string) uint64 {
	var stat syscall.Stat_t
	if err := syscall.Stat(filename, &stat); err != nil {
		log.Println("File was not found, passing...")
	}
	log.Printf("Inode: %d\n", stat.Ino)
	return stat.Ino
}

func fileOpen(filename string, startPosition string) (*os.File, *FileState) {
	fileStateOld, err := getFileState(filename)
	if err != nil {
		log.Debugf("Could not get file state for %v", filename)
	}
	var f *os.File
	for {
		file, err := os.Open(filename)
		if err != nil {
			log.WithFields(log.Fields{"filename": filename, "err": err}).Warn("Could not open file")
			time.Sleep(3 * time.Second)
		} else {
			log.WithFields(log.Fields{"filename": filename}).Info("File opened successfully")
			f = file
			break
		}
	}
	fi, _ := os.Stat(filename)
	if fileStateOld != nil && fileStateOld.Inode == getIno(fi) {
		// If old file state is still actual
		log.Info("Old file state is actual")
		f.Seek(fileStateOld.Position, 0)
		return f, fileStateOld
	}
	fileState := &FileState{
		FilePath: filename,
		FileInfo: fi,
		Inode:    0,
		Position: 0,
	}
	if startPosition == "end" {
		f.Seek(0, 2) // Seeking to the end of the file
		fileState.Position = fi.Size()
	}
	stat := fi.Sys().(*syscall.Stat_t)
	fileState.Inode = stat.Ino
	fileState.Save()
	return f, fileState

}

func FileTailer(filename string, queue chan StringWrapper, startPos string) {

	f, fileState := fileOpen(filename, startPos)
	fi, err := os.Stat(filename)
	if err != nil {
		log.WithError(err).Errorf("Failed to open file: %v", filename)
		return
	}

	var buffer bytes.Buffer
	lineBuffer := ""
	hostname, _ := os.Hostname()
	log.Debugf("Hostname: %v", hostname)
	now := time.Now()

	const ReadSize = 2048
	for {
		// Check if file was deleted
		fiNew, err := os.Stat(filename)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Warn("Problem with getting file info, re-opening file")
			f, fileState = fileOpen(filename, "beggining") // Open new files from the start
			continue
		}
		// Check if file was replaced by other file
		if !os.SameFile(fi, fiNew) {
			fi = fiNew
			log.Info("File was replaced with other file, need to reopen file")
			f, fileState = fileOpen(filename, "beggining") // Open new files from the start

			continue
		}
		// Check if file was truncated
		if fiNew.Size() < fi.Size() {
			log.WithFields(log.Fields{"old_size": fi.Size(), "new_size": fiNew.Size()}).Info("Looks like file was truncated, reopening")
			fi = fiNew
			f, fileState = fileOpen(filename, "beggining") // Open truncated files from the start
			f.Seek(0, 0)
			fileState.Position = 0
		}
		// All checks passed
		fi = fiNew

		for i := 0; i < 3; i++ {
			toRead := make([]byte, ReadSize)
			readCount, err := f.Read(toRead)
			log.Debugf("Readed %v bytes", readCount)
			if readCount > 0 {
				_, err = buffer.Write(toRead[:readCount])
				check(err)
			}
			fileState.Position += int64(readCount)
			if err != nil && err != io.EOF {
				check(err)
				break
			}
			if err != nil && err == io.EOF {
				log.Debugln("Reached end of file, sleeping more...")
				time.Sleep(1 * time.Second)
			}
		}

		for {
			readString, err := buffer.ReadString('\n')
			if err != nil && err == io.EOF {
				log.Debug("Nothing to read in the buffer")
				break
			}

			lineBuffer += readString
			log.Debugf("Line Buffer: %v", lineBuffer)

			log.WithFields(log.Fields{"string": lineBuffer}).Debug("Read string")
			line := lineBuffer[:len(lineBuffer)-1]
			meta := map[string]string{
				"path": filename,
				"host": hostname,
			}
			log.Debugf("Sending to log: %v", line)
			queue <- StringWrapper{line, meta}
			lineBuffer = ""

		}

		if (time.Since(now)).Seconds() > 5 {
			now = time.Now()
			log.Debugln("Passed 5 seconds, saving file state")
			fileState.Save()
		}
	}
}
