package main

import (
	"encoding/json"
	"os"

	"github.com/Sirupsen/logrus"
)

type FileConfig struct {
	StartPosition string
	Path          string
}
type ESConfig struct {
	URL      string
	User     string
	Password string
}

type Configuration struct {
	Files          []FileConfig
	LineProcessors int
	ESSenders      int
	ESBulkSize     int
	FileType       string // cdn or revive
	LogLevel       string
	IndexFormat    string
	ESConfig       ESConfig
}

func GetConfig() Configuration {
	file, err := os.Open("conf.json")
	if err != nil {
		logrus.Panicf("Failed to open config file: %v", err)
	}

	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		logrus.Panicf("Failed to parse config file: %v", err)
	}

	return configuration
}
