package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
)

// Mon Jan 2 15:04:05 -0700 MST 2006
const ESTimeFormat = "2006-01-02T15:04:05-0700"

type ESClient struct {
	ESUrl      string
	Password   string
	User       string
	HTTPClient *http.Client
}

type BulkAction struct {
	Action string
	Index  string
	Type   string
	Id     string
	Fields map[string]string
}

func (action *BulkAction) GetMetaJSON() ([]byte, error) {
	m := map[string]map[string]string{
		action.Action: {
			"_index": action.Index,
			"_type":  action.Type,
		},
	}
	jsonString, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return jsonString, nil
}
func (action *BulkAction) GetDataJSON() ([]byte, error) {
	jsonString, err := json.Marshal(action.Fields)
	if err != nil {
		return nil, err
	}

	return jsonString, nil
}

func (action *BulkAction) GetBulkData() ([]byte, error) {
	meta, err := action.GetMetaJSON()
	if err != nil {
		return meta, err
	}
	data, err := action.GetDataJSON()
	if err != nil {
		return data, err
	}
	//log.Debugf("Getting bulk data for %v", action)
	//log.Debugf("meta: %v", string(meta))
	//log.Debugf("data: %v", string(data))
	// result := append(meta, []byte("\n")) // + data + []byte("\n")
	result := append(meta, '\n')
	result = append(result, data...)
	result = append(result, '\n')
	//log.Debugf("TYpe: %T, str: %v", result, string(result))
	//fmt.Print(string(result))
	return result, nil

}

func (esClient *ESClient) BulkIndex(documents []*BulkAction) {
	//esClient.HTTPClient.Post("http://es.itftd.com/_bulk", "application/data")
	// var str_body string
	var body []byte
	if esClient.Password == "" {
		log.Warn("Empty client password, please investigate")
	}

	for _, bulkAction := range documents {
		if bulkAction == nil {
			log.Warn("Action is nil, why, oh my god why???")
			continue
		}
		data, err := bulkAction.GetBulkData()
		if err != nil {
			log.Warnf("Oops, %v", err)
			continue
		}
		body = append(body, data...)
	}
	req, err := http.NewRequest("POST", esClient.ESUrl+"/_bulk", bytes.NewReader(body))
	req.SetBasicAuth(esClient.User, esClient.Password)

	now := time.Now()
	log.Infof("Doing request: %+v", req.URL.String())
	resp, err := esClient.HTTPClient.Do(req)
	if err != nil {
		log.Errorf("Error when posting data to ES: %v", err)
		return
	}

	log.Infof("Posted to es: %v, took: %v", resp.StatusCode, time.Since(now))
	defer resp.Body.Close()
	contents, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("%s", err)
	}
	if resp.StatusCode != 200 {
		log.Errorf("Response code from ES is %d", resp.StatusCode)
		log.Errorf("Body was: %v", string(contents))
	}
}

func ESSender(reqChan chan RequestData, indexFormat string) {
	//counter, thousandCounter := 0, 0
	counter := 0
	tickChan := time.NewTicker(10 * time.Second)
	config := GetConfig()
	bulkSize := config.ESBulkSize
	documents := make([]*BulkAction, 0, bulkSize)
	esClient := ESClient{
		ESUrl:      config.ESConfig.URL,
		User:       config.ESConfig.User,
		Password:   config.ESConfig.Password,
		HTTPClient: new(http.Client),
	}

	for {
		select {
		case req := <-reqChan:
			log.Debugln("Received some parsed Request")

			bulkAction := new(BulkAction)
			bulkAction.Fields = req.getData()
			bulkAction.Index = time.Now().Format(indexFormat)
			bulkAction.Action = "index"
			bulkAction.Type = "logs"
			documents = append(documents, bulkAction)
			counter += 1

			if counter == bulkSize {
				counter = 0
				esClient.BulkIndex(documents)
				documents = make([]*BulkAction, 0, bulkSize)
			}
		case <-tickChan.C:
			log.Debugln("Flushing ES queue because of time ticker")
			if counter > 0 {
				counter = 0
				esClient.BulkIndex(documents)
				documents = make([]*BulkAction, 0, bulkSize)
			}
		}
	}
}
