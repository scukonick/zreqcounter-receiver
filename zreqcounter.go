package main

import (
	"time"

	log "github.com/Sirupsen/logrus"
)

import (
	_ "net/http/pprof"
	"github.com/sirupsen/logrus"
)

func main() {

	config := GetConfig()
	switch config.LogLevel {
	case "DEBUG":
		log.SetLevel(log.DebugLevel)
	case "WARN":
		log.SetLevel(log.WarnLevel)
	case "INFO":
		log.SetLevel(log.InfoLevel)
	default:
		log.SetLevel(log.InfoLevel)
	}

	var fType int
	switch config.FileType {
	case "cdn":
		fType = cdnLog
	case "revive":
		fType = reviveLog
	default:
		logrus.Warningf("Invalid file type: %v, assuming cdn", config.FileType)
		fType = cdnLog
	}

	stringCh := make(chan StringWrapper)
	requestCh := make(chan RequestData)
	for _, fileConfig := range config.Files {
		go FileTailer(fileConfig.Path, stringCh, fileConfig.StartPosition)
	}
	for i := 0; i < config.LineProcessors; i++ {
		go LineProcessor(stringCh, requestCh, fType)
	}
	for i := 0; i < config.ESSenders; i++ {
		go ESSender(requestCh, config.IndexFormat)
	}
	for {
		time.Sleep(1 * time.Second)
	}
}
