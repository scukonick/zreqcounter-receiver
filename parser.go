package main

import (
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
)

const TimeLayout = "02/Jan/2006:15:04:05 Z0700"

const (
	cdnLog = iota
	reviveLog
)

type RequestData struct {
	Ip        string
	UrlParams url.Values
	Request   string // URL with parameters
	Uri       string // URL without parameters
	TimeStamp time.Time
	UA        string
	UARaw     string
	GeoCF     string
	HTTPHost  string
	Response  string
	Meta      map[string]string
	fType     int
}

func (req RequestData) getData() map[string]string {
	data := map[string]string{
		"client":       req.Ip,
		"ua_parsed":    req.UA,
		"useragent":    req.UARaw,
		"geocf":        req.GeoCF,
		"http_host":    req.HTTPHost,
		"response":     req.Response,
		"request":      req.Request,
		"uri":          req.Uri,
		"newtimestamp": req.TimeStamp.Format(ESTimeFormat),
	}
	for param, values := range req.UrlParams {
		jsParam := "param_" + param
		data[jsParam] = values[0]
	}

	// Backwards compatibility params
	if strings.Contains(data["request"], "/px.png") {
		if sParams, ok := req.UrlParams["s"]; ok {
			data["partner"] = sParams[0]
		}
		if chaParams, ok := req.UrlParams["cha"]; ok {
			data["cha"] = chaParams[0]
		}
		if saidParams, ok := req.UrlParams["sa"]; ok {
			data["said"] = saidParams[0]
		}
		if iParams, ok := req.UrlParams["i"]; ok {
			data["i"] = iParams[0]
		}
	}

	// Meta Info
	for param, value := range req.Meta {
		data[param] = value
	}

	switch req.fType {
	case cdnLog:
		data["tags"] = "cdn_nginx_request"
	case reviveLog:
		data["tags"] = "revive_nginx_request"

		locDomain, err := req.getLocDomain()
		if err != nil {
			log.Warningf("Failed to get loc domain: %+v", err)
			break
		}
		data["loc_domain"] = locDomain
	}
	return data
}

// getLocDomain returns domain part from loc get parameter
func (req RequestData) getLocDomain() (string, error) {
	loc, ok := req.UrlParams["loc"]
	if !ok {
		return "", errors.New("failed to get loc param")
	}

	locUrl, err := url.Parse(loc[0])
	if err != nil {
		return "", fmt.Errorf("failed to parse loc url: %+v", err)
	}

	domain := locUrl.Host
	domain = strings.TrimPrefix(domain, "www.")

	return domain, nil
}

// Example
// 89.0.204.21"-"20/Sep/2015:04:47:25 +0000"-"/img/px.png?s=e&cha=2910&sa=56001&_1=1442724&_2=da8ffa8103fa681dc9626df21b51a076&i=3&"-"200"-"Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko"-"DE"-"cdnone.net

type Parser struct {
	fileType int
}

func NewParser(fileType int) *Parser {
	return &Parser{
		fileType: fileType,
	}
}

func ParseLogLine(line string, fType int) (RequestData, error) {
	splitString := strings.Split(line, "\"-\"")
	if len(splitString) != 7 {
		log.Warn("Could not parse line")
		return RequestData{}, errors.New("could not parse log record")
	}
	var err error
	var result RequestData
	result.Ip = splitString[0]
	result.Request = splitString[2]
	result.Response = splitString[3]
	result.UARaw = splitString[4]
	result.GeoCF = splitString[5]
	result.HTTPHost = splitString[6]
	result.TimeStamp, err = time.Parse(TimeLayout, splitString[1])
	result.fType = fType
	if err != nil {
		log.Warnf("Failed to parse time: %v", splitString[1])
		return RequestData{}, err
	}
	result.UA = GetUserAgent(result.UARaw)

	parsedUrl, err := url.Parse(result.Request)
	//	fmt.Println("Parsed url: ")
	//	fmt.Println(parsedUrl)
	if err != nil { // If we can not parse url return error
		log.Warnf("Failed to parse URL: %v", result.Request)
		return RequestData{}, err
	}

	query := parsedUrl.RawQuery

	result.UrlParams, err = url.ParseQuery(query)
	if err != nil {
		log.Warnf("Failed to parse line: %+v", err)
		return RequestData{}, err
	}
	result.Uri = parsedUrl.Path

	return result, nil
}

func GetUserAgent(usString string) string {
	switch {
	case strings.Contains(usString, "Chrome"):
		return "Chrome"
	case strings.Contains(usString, "Safari"):
		return "Safari"
	case strings.Contains(usString, "Firefox"):
		return "Firefox"
	case strings.Contains(usString, "Opera"):
		return "Opera"
	case checkIE11(usString, ie11Regexp):
		return "IE_11"
	case strings.Contains(usString, "MSIE"):
		return getIEWithVersion(usString, ieRegexp)
	default:
		return "Other"
	}
}

var (
	ieRegexp   = regexp.MustCompile(`^.*Trident.*rv:11.0`)
	ie11Regexp = regexp.MustCompile(`^.*MSIE (\d+).*`)
)

func checkIE11(uaString string, ieRegexp *regexp.Regexp) bool {
	results := ieRegexp.FindStringSubmatch(uaString)

	return results != nil
}

func getIEWithVersion(uaString string, ieRegexp *regexp.Regexp) string {
	results := ieRegexp.FindStringSubmatch(uaString)
	if results != nil {
		return fmt.Sprintf("IE_%v", results[1])
	}

	return "Other"
}

func LineProcessor(inputCh chan StringWrapper, outputCh chan RequestData, fType int) {

	for {
		lineWrapper := <-inputCh
		//		log.Printf("Received line: %v", line)
		if fType == reviveLog && !strings.Contains(lineWrapper.line, "/p/lg.php") {
			continue
		}

		results, err := ParseLogLine(lineWrapper.line, fType)
		if err != nil {
			log.Printf("Oops, could not parse line..")
			continue
		}
		results.Meta = lineWrapper.meta

		outputCh <- results
	}
}
